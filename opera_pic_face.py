# coding: utf-8

from pywinauto.application import Application


def open_sg_pic(process):
    sg_app = Application(backend='uia').connect(process=process)
    sg_win = sg_app.window(class_name='CherryWindowClass')
    sg_win.print_control_identifiers()
