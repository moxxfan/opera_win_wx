# coding: utf-8
import time

import keyboard
from pywinauto.application import Application


def open_wx_main_win(process, send_msg: str, send_times: int):
    wx_app = Application(backend='uia').connect(process=process)
    wx_app.top_window()

    wx_win = wx_app.window(class_name='ChatWnd')

    wx_msg_input = wx_win.child_window(title="输入", control_type="Edit")

    wx_win.click_input()

    # 聚焦到文本框
    wx_msg_input.click_input()
    for i in range(send_times):
        keyboard.write(f'{send_msg}-{i}')
        keyboard.send('enter')
        time.sleep(0.1)


if __name__ == '__main__':
    open_wx_main_win(process=7328,
                     send_msg=u'多喝热水',
                     send_times=66)
